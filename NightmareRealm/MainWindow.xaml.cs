﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Controls;
using static NightmareRealm.Field;
using System.IO.Packaging;
using System.Linq;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization;
using System.Xml;
using System.Text.Json;
using Newtonsoft.Json;
using System.Text;
using System.Windows.Markup;

namespace NightmareRealm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string greenImgPath = @"\Resources\green.jpg";
        private string orangeImgPath = @"\Resources\orange.jpg";
        private string purpleImgPath = @"\Resources\purple.jpg";
        private string crossImgPath = @"Resources\cross.jpg";
        private string emptyImgPath = @"Resources\empty.jpg";

        private string greenBorderedImgPath = @"\Resources\green_bordered.jpg";
        private string orangeBorderedImgPath = @"\Resources\orange_bordered.jpg";
        private string purpleBorderedImgPath = @"\Resources\purple_bordered.jpg";

        private Dictionary<CellType, string[]> ImgDict; 


        private int currentRow = 1;
        private int currentColumn = 0;
        private Field field;

        private Config? config=null;
        public MainWindow()
        {
            InitializeComponent();

            Init();

            Show_Rules();

            //SaveState();

            //Config? config = GetSavedState();
            
        }
        public void Init()
        {
            // winning sequence 
            //List<char> values = new List<char>() { 'F', 'F', 'F', 'S', 'F', 'X', '\0', '\0', '\0', '\0', 'S', 'F', 'S', 'S', 'S', 'X', 'X', 'X', 'X', 'X', 'T', 'T', 'T', 'T', 'T' };
            
            List<char> columnOrder = new List<char>() { 'F', 'S', 'T' };
            ImgDict = new Dictionary<CellType, string[]>();
            ImgDict.Add(CellType.Empty,new string[]{ emptyImgPath, emptyImgPath });
            ImgDict.Add(CellType.Cross, new string[] { crossImgPath, crossImgPath });
            for (int i=0;i<columnOrder.Count;i++)
            {
                switch(columnOrder[i])
                {
                    case 'F':
                        ImgDict.Add(CellType.FirstType,new string[] { greenImgPath, greenBorderedImgPath });
                        break;
                    case 'S':
                        ImgDict.Add(CellType.SecondType, new string[] { orangeImgPath, orangeBorderedImgPath });
                        break;
                    case 'T':
                        ImgDict.Add(CellType.ThirdType, new string[] { purpleImgPath, purpleBorderedImgPath });
                        break;
                }
            }
            SetImgColor(0, 0, CellType.FirstType);
            SetImgColor(0, 2, CellType.SecondType);
            SetImgColor(0, 4, CellType.ThirdType);

            List<char> values;

            if (config == null)
            {
                values = StartupGenerator.GenerateField(); //new List<char>() { 'F','F','S','T','T', '\0','X','X', '\0', 'X','F','S','T','S','S','X', '\0', '\0', 'X','X','T','T','F','F','S'};
            }
            else
            {
                values = config._currentState;
            }

            field = new Field();
            field.init(values);
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    SetImgColor(i + 1, j, field.Table[i, j]);    
                }
            }
        }
        public void SaveState()
        {
            List<char> currentState = new List<char>();
            CellType[,] transponCellTable = new CellType[5, 5];
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++)
                    transponCellTable[i, j] = field.Table[j, i];
            foreach (CellType cellType in transponCellTable)
            {
                switch (cellType)
                { 
                    case CellType.FirstType: currentState.Add('F'); break;
                    case CellType.SecondType: currentState.Add('S'); break;
                    case CellType.ThirdType: currentState.Add('T'); break;
                    case CellType.Cross: currentState.Add('X'); break;
                    case CellType.Empty: currentState.Add('\0'); break;
                }
            }
            //Config config = new Config(currentState);
            Config newConfig = new Config(currentState);
            File.WriteAllText("config.json", JsonConvert.SerializeObject(newConfig, Newtonsoft.Json.Formatting.Indented), Encoding.UTF8);
        }
        public Config GetSavedState()
        {
            return JsonConvert.DeserializeObject<Config>(File.ReadAllText("config.json"));
        }
        private void SetImgColor(int row, int column, CellType cellType)
        {
            Border border = new Border { BorderBrush = Brushes.Black};
            Image img;
            if (row != 0)
            {
                img = new Image
                {
                    Source = new BitmapImage(new Uri(ImgDict[cellType][0]/*imgPath*/, UriKind.Relative)),
                    Stretch = System.Windows.Media.Stretch.Fill,

                };
                img.MouseLeftButtonDown += (s, e) =>
                {
                    if (currentRow != 0)
                        ChangeImgColor(currentRow, currentColumn, ImgDict[field.Table[currentRow-1, currentColumn]][0]);
                    currentRow = Grid.GetRow(img);
                    currentColumn = Grid.GetColumn(img);
                    img.Source = new BitmapImage(new Uri(ImgDict[field.Table[currentRow-1, currentColumn]][1], UriKind.Relative));
                };
            }
            else
            {
                img = new Image
                {
                    Source = new BitmapImage(new Uri(ImgDict[cellType][1]/*imgPath*/, UriKind.Relative)),
                    Stretch = System.Windows.Media.Stretch.Fill,

                };
            }
            Grid.SetRow(img, row);
            Grid.SetColumn(img, column);
            GameGrid.Children.Add(img);
            
        }
        public BitmapImage GetImageSource(string uriString)
        {
            BitmapImage bi3 = new BitmapImage();
            bi3.BeginInit();
            bi3.UriSource = new Uri(uriString, UriKind.Relative);
            bi3.EndInit();
            return bi3;
        }
        private void User_Key_Down(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (currentRow == 0) return;
            switch (e.Key.ToString())
            {
                case "Right":
                    Right(currentRow - 1, currentColumn);
                    break;
                case "Left":
                    Left(currentRow - 1, currentColumn);
                    break;
                case "Up":
                    Up(currentRow - 1, currentColumn);
                    break;
                case "Down":
                    Down(currentRow - 1, currentColumn);
                    break;
            }
            //MessageBox.Show(e.Key + " = " + e.Key.ToString());
        }
        private void ChangeImgColor(int row, int column, string imgPath)
        {
            if (row != 0 /*&& !GameGrid.Children.Equals(Menu)*/)
            {
                GameGrid.Children.Cast<Image>()
                .First(e => Grid.GetRow(e) == row && Grid.GetColumn(e) == column)
                .Source = new BitmapImage(new Uri(imgPath, UriKind.Relative));
            }
        }
        private void CheckWin()
        {
            if(field.winStatus(CellType.FirstType, CellType.SecondType, CellType.ThirdType))
            {
                MessageBox.Show("Поздравляю, ты выиграл!!!");
                return;
            }
        }
        public void Right(int x, int y)
        {
            if ((field.Table[x, y] == CellType.FirstType || field.Table[x, y] == CellType.SecondType 
                || field.Table[x, y] == CellType.ThirdType) && (x < 5) && (x >= 0) && (y < 5) && (y >= 0))
            {
                if (y + 1 < 5)
                {
                    if (field.Table[x, y + 1] == CellType.Empty)
                    {
                        field.Table[x, y + 1] = field.Table[x, y];
                        field.Table[x, y] = CellType.Empty;
                        ChangeImgColor(currentRow, currentColumn, emptyImgPath);
                        switch(field.Table[x, y + 1])
                        {
                            case CellType.FirstType:
                                ChangeImgColor(currentRow, currentColumn + 1, greenBorderedImgPath);
                                break;
                            case CellType.SecondType:
                                ChangeImgColor(currentRow, currentColumn + 1, orangeBorderedImgPath);
                                break;
                            case CellType.ThirdType:
                                ChangeImgColor(currentRow, currentColumn + 1, purpleBorderedImgPath);
                                break;
                        }
                        currentColumn++;
                        CheckWin();
                    }
                    else
                    {
                        MessageBox.Show("Клетка справа занята");
                    }
                }
                else
                {
                    MessageBox.Show("Сделать шаг вправо невозможно");
                }
            }
            else
            {
                MessageBox.Show("Эту клетку невозможно передвинуть");
            }
        }
        public void Left(int x, int y)
        {
            if ((field.Table[x, y] == CellType.FirstType || field.Table[x, y] == CellType.SecondType 
                || field.Table[x, y] == CellType.ThirdType) && (x < 5) && (x >= 0) && (y < 5) && (y >= 0))
            {
                if (y - 1 >= 0)
                {
                    if (field.Table[x, y - 1] == CellType.Empty)
                    {
                        field.Table[x, y - 1] = field.Table[x, y];
                        field.Table[x, y] = CellType.Empty;
                        ChangeImgColor(currentRow, currentColumn, emptyImgPath);
                        switch (field.Table[x, y - 1])
                        {
                            case CellType.FirstType:
                                ChangeImgColor(currentRow, currentColumn - 1, greenBorderedImgPath);
                                break;
                            case CellType.SecondType:
                                ChangeImgColor(currentRow, currentColumn - 1, orangeBorderedImgPath);
                                break;
                            case CellType.ThirdType:
                                ChangeImgColor(currentRow, currentColumn - 1, purpleBorderedImgPath);
                                break;
                        }
                        currentColumn--;
                        CheckWin();
                    }
                    else
                    {
                        MessageBox.Show("Клетка слева занята");
                    }
                }
                else
                {
                    MessageBox.Show("Сделать шаг влево невозможно");
                }
            }
            else
            {
                MessageBox.Show("Эту клетку невозможно передвинуть");
            }
        }
        public void Up(int x, int y)
        {
            if ((field.Table[x, y] == CellType.FirstType || field.Table[x, y] == CellType.SecondType 
                || field.Table[x, y] == CellType.ThirdType) && (x < 5) && (x >= 0) && (y < 5) && (y >= 0))
            {
                if (x - 1 >= 0)
                {
                    if (field.Table[x - 1, y] == CellType.Empty)
                    {
                        field.Table[x - 1, y] = field.Table[x, y];
                        field.Table[x, y] = CellType.Empty;
                        ChangeImgColor(currentRow, currentColumn, emptyImgPath);
                        switch (field.Table[x - 1, y])
                        {
                            case CellType.FirstType:
                                ChangeImgColor(currentRow - 1, currentColumn, greenBorderedImgPath);
                                break;
                            case CellType.SecondType:
                                ChangeImgColor(currentRow - 1, currentColumn, orangeBorderedImgPath);
                                break;
                            case CellType.ThirdType:
                                ChangeImgColor(currentRow - 1, currentColumn, purpleBorderedImgPath);
                                break;
                        }
                        currentRow--;
                        CheckWin();
                    }
                    else
                    {
                        MessageBox.Show("Клетка сверху занята");
                    }
                }
                else
                {
                    MessageBox.Show("Сделать шаг вверх невозможно");
                }
            }
            else
            {
                MessageBox.Show("Эту клетку невозможно передвинуть");
            }
        }
        public void Down(int x, int y)
        {
            if ((field.Table[x, y] == CellType.FirstType || field.Table[x, y] == CellType.SecondType 
                || field.Table[x, y] == CellType.ThirdType) && (x < 5) && (x >= 0) && (y < 5) && (y >= 0))
            {
                if (x + 1 < 5)
                {
                    if (field.Table[x + 1, y] == CellType.Empty)
                    {
                        field.Table[x + 1, y] = field.Table[x, y];
                        field.Table[x, y] = CellType.Empty;
                        ChangeImgColor(currentRow, currentColumn, emptyImgPath);
                        switch (field.Table[x + 1, y])
                        {
                            case CellType.FirstType:
                                ChangeImgColor(currentRow + 1, currentColumn, greenBorderedImgPath);
                                break;
                            case CellType.SecondType:
                                ChangeImgColor(currentRow + 1, currentColumn, orangeBorderedImgPath);
                                break;
                            case CellType.ThirdType:
                                ChangeImgColor(currentRow + 1, currentColumn, purpleBorderedImgPath);
                                break;
                        }
                        currentRow++;
                        CheckWin();
                    }
                    else
                    {
                        MessageBox.Show("Клетка снизу занята");
                    }
                }
                else
                {
                    MessageBox.Show("Сделать шаг вниз невозможно");
                }
            }
            else
            {
                MessageBox.Show("Эту клетку невозможно передвинуть");
            }
        }

        private void Save_State_Click(object sender, RoutedEventArgs e)
        {
            SaveState();
        }

        private void Restore_State_Click(object sender, RoutedEventArgs e)
        {
            if(File.Exists("config.json"))
            {
                GameGrid.Children.RemoveRange(0, GameGrid.Children.Count);
                config = GetSavedState();
                Init();
            }
            else
            {
                MessageBox.Show("Предыдущей игры не существует");
            }
        }

        private void Start_New_Game_Click(object sender, RoutedEventArgs e)
        {
            GameGrid.Children.RemoveRange(0, GameGrid.Children.Count);
            config = null;
            Init();
        }
        public void Show_Rules()
        {
            MessageBox.Show("Перемещение клеток стрелками\nВыбор клетки производится нажатием левой кнопкой мыши по клетке\n" +
                "Цель игры: Расставить клетки в полонны под соответствующим цветом.\nПеремещать можно только цветные клетки на свободные места. " +
                "Заблокированые клетки перемещать нельзя. \nМеню раскрывается после нажатия на правую кнопку мыши\n" +
                "\nДля продолжения закройте это окно");
        }
    }
}
