﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NightmareRealm
{
    public class StartupGenerator
    {
        public static List<char> GenerateField()
        {
            //Dictionary<int,char> cardDict = new Dictionary<int,char>();
            List<char> acceptableValuesCards = new List<char> { 'F', 'S', 'T' };
            List<char> acceptableValuesXorEmpty = new List<char> { 'X', '\0' };

            int countF = 5;
            int countS = 5;
            int countT = 5;
            int countX = 6;
            int countEmpty = 4;

            int?[] cards = new int?[25];

            for (int i = 0; i < cards.Length; i++)
            {
                cards[i] = i;
            }

            char[] result = new char[25];
            Random rnd = new Random();
            //Console.WriteLine(rnd.Next(0,0));

            while (countF + countS + countT + countX + countEmpty != 0)
            {

                int randomIndex = rnd.Next(0, cards.Length);
                if (cards[randomIndex] != null)
                {
                    if (countF == 0)
                    {
                        acceptableValuesCards.Remove('F');
                    }
                    if (countS == 0)
                    {
                        acceptableValuesCards.Remove('S');
                    }
                    if (countT == 0)
                    {
                        acceptableValuesCards.Remove('T');
                    }
                    if ((cards[randomIndex] / 5) % 2 == 0)
                    {

                        int randomChar = -1;
                        if (acceptableValuesCards.Count != 0)
                            randomChar = rnd.Next(0, acceptableValuesCards.Count);
                        if (randomChar == -1)
                            break;
                        result[randomIndex] = acceptableValuesCards[randomChar];
                        cards[randomIndex] = null;
                        switch (acceptableValuesCards[randomChar])
                        {
                            case 'F': if (countF != 0) countF--; break;
                            case 'S': if (countS != 0) countS--; break;
                            case 'T': if (countT != 0) countT--; break;
                        }

                    }
                    else
                    {
                        if (countX == 0)
                        {
                            acceptableValuesXorEmpty.Remove('X');
                        }
                        if (countEmpty == 0)
                        {
                            acceptableValuesXorEmpty.Remove('\0');
                        }
                        int randomChar = rnd.Next(0, acceptableValuesXorEmpty.Count);
                        result[randomIndex] = acceptableValuesXorEmpty[randomChar];
                        cards[randomIndex] = null;
                        switch (acceptableValuesXorEmpty[randomChar])
                        {
                            case 'X':
                                if (countX != 0)
                                    countX--; break;
                            case '\0':
                                if (countEmpty != 0)
                                    countEmpty--; break;
                        }

                    }
                }
            }
            return result.ToList<char>();

        }
    }
}
