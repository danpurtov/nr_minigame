﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace NightmareRealm
{
    public class Config
    {
        public List<char> _currentState { get; set; }
        [JsonConstructor]
        public Config(List<char> currentState)
        {
            this._currentState = currentState;
        }
    }
}
