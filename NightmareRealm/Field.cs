﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NightmareRealm
{
    public class Field
    {
        public enum CellType
        {
            FirstType,
            SecondType,
            ThirdType,
            Cross,
            Empty
        }
        public CellType[,] Table { get; set; }
        public Field()
        {
            Table = new CellType[5, 5];
        }
        public bool winStatus(CellType firstType, CellType secondType, CellType thirdType)
        {
            for (int i = 0; i < 5; i++)
            {
                if (!((Table[i, 0] == firstType) && (Table[i, 2] == secondType) && (Table[i, 4] == thirdType)))
                    return false;
            }
            return true;
        }
        public void init(List<char> values) //на вход подаются значения клеток по порядку сверху-вниз слева-направо
        {
            int[] colorColumns = new int[] { 0, 2, 4 };
            int i = 0;
            int j = 0;
            foreach (char value in values)
            {
                switch (value)
                {
                    case 'F':
                        Table[j, i] = CellType.FirstType;
                        break;
                    case 'S':
                        Table[j, i] = CellType.SecondType;
                        break;
                    case 'T':
                        Table[j, i] = CellType.ThirdType;
                        break;
                    case 'X':
                        Table[j, i] = CellType.Cross;
                        break;
                    case '\0':
                        Table[j, i] = CellType.Empty;
                        break;
                }
                j++;
                if (j == 5)
                {
                    j = 0;
                    i++;
                }
            }
        }
    }
}
